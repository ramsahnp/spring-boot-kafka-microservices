package com.ram.kafka.orderservice.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

    @Value("${spring.kafka.topic.name}")
    private String topicName;

    //we can set partition and when we start this microservice this topic name will be created in kafka topic
    @Bean
    public NewTopic topic() {
        return TopicBuilder.name(topicName).build();
    }


}
