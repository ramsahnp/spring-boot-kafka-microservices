package com.ram.kafka.stockservice.kafka;

import com.ram.kafka.basedomain.dto.OrderEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class OrderConsumer {

    private static final Logger logger = LoggerFactory.getLogger(OrderConsumer.class);

    @KafkaListener(topics = "${spring.kafka.topic.name}", groupId = "${spring.kafka.consumer.group-id}")
    public void consumerOrder(OrderEvent orderEvent) {
        logger.info(String.format("Order Event Received in Stock Service ==> %s", orderEvent.toString()));

        //we can write code for saving data to the database here..
    }
}
